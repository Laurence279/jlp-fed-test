import styles from "./product-carousel.module.scss";
import { useState } from "react";

const ProductCarousel = ({ images }) => {

  const [imageIndex, setImageIndex] = useState(0)
  
  function getSelectedStyle(index) {
    if(imageIndex !== index) return {}
    return {
      backgroundColor: "black"
    } 
  }

  function handleClick(index) {
    setImageIndex(index)
  }

  return (
    <div className={styles.productCarousel} aria-roledescription="carousel">
      <img src={images[imageIndex]} alt="image of product" style={{ width: "100%", maxWidth: "500px" }} />
      <div className={styles.carouselSelectors}>
        <div onClick={() => handleClick(0)} style={getSelectedStyle(0)}></div>
        <div onClick={() => handleClick(1)} style={getSelectedStyle(1)}></div>
        <div onClick={() => handleClick(2)} style={getSelectedStyle(2)}></div>
      </div>
    </div>
  );
};

export default ProductCarousel;
