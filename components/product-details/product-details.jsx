import styles from "./product-details.module.scss";
import ProductCarousel from "../product-carousel/product-carousel";

const ProductDetails = ({ data }) => {


  if(!data.media) return <div style={{textAlign: "center"}}>
      <p>Coming soon! Please see other products in our range!</p>
  </div>

  function getAttributesToDisplay(data) {

          // Return:
            // everything that has a "YES" value.
            // dimensions - attr80000016338
            // Drying performance - attr80000016355
            // Drying system - attr80000016357

    const attributeList = new Set(["attr80000016338", "attr80000016355", "attr80000016357"])

    // Get two lists of attributes separated by values

    const yesAttributes = [];
    const otherAttributes = [];
    data.details.features[0].attributes.forEach((item) => {

        if(item.value === "YES"){
          yesAttributes.push(item)
          return;
        }

        if(attributeList.has(item.id)){
            otherAttributes.push(item)
            return;  
        }

    })

    // Return "YES" attributes followed by other..
    return [...yesAttributes, ...otherAttributes]
    

  }

  return (
    <div className={styles.content}>

        <ProductCarousel images={data.media.images.urls} />

      

      <div className={styles.purchaseInfo}>
          <h1 className={styles.price} data-testid="price">£{data.price.now}</h1>
          <h4 className={styles.specialOffer} data-testid="special-offer">{data.displaySpecialOffer}</h4>
          <h4 className={styles.services} data-testid="services">{data.additionalServices.includedServices}</h4>
        </div>

    <div className={styles.informationContainer}>

        <div className={styles.productInfoContainer}>
          <h2>Product information</h2>
          <div className={styles.productInfoContent}>
            <div dangerouslySetInnerHTML={{__html: data.details.productInformation.split("</p>")[0]}}></div>
            <p>Product code: {data.productId}</p>
          </div>
        </div>
        <h2>Product specification</h2>
        <ul  data-testid="features">
          {getAttributesToDisplay(data).map((item,index) => (

            <li className={styles.attribute} key={`${item.name}${index}`}>
                <p dangerouslySetInnerHTML={{ __html: item.name }}></p>
                <p dangerouslySetInnerHTML={{ __html: item.value }}></p>
            </li>

            
          ))}
        </ul>
      </div>

    </div>

  );
};

export default ProductDetails;
