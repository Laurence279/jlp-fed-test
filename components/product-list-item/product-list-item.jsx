import styles from "./product-list-item.module.scss";

const ProductListItem = ({ image, price, description }) => {
  return (
    <div className={styles.content}  data-testid="product-list-item">
      <div>
        <img src={image} alt="" style={{ width: "100%" }} />
      </div>
      <div className={styles.caption}>
        <p>{description}</p>
        <p className={styles.price}>£{price}</p>
      </div>
    </div>
  );
};

export default ProductListItem;
