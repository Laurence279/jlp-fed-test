import Head from "next/head";
import Link from "next/link";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";
import mockData from "../mockData/data.json"

const MAX_ITEMS_DISPLAYED = 20;

// export async function getServerSideProps() {
//   const response = await fetch(
//     "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
//   );
//   const data = await response.json();
//   return {
//     props: {
//       data: data,
//     },
//   };
// }

export function getServerSideProps() {
  return {
    props: {
      data: mockData
    }
  }
}

const Home = ({ data }) => {

  let items = data.products.slice(0, MAX_ITEMS_DISPLAYED);
  return (
    <div data-testid="home-page">
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <h1>Dishwashers ({items.length})</h1>
        <div className={styles.content}  data-testid="product-list-items">
          {items.map((item) => (
            <Link
              key={item.productId}
              href={{
                pathname: "/product-detail/[id]",
                query: { id: item.productId },
              }}
            >
              <a className={styles.link}>

                <ProductListItem key={`product_${item.productId}`} image={item.image} price={item.price.now} description={item.title} />
                
              </a>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;
