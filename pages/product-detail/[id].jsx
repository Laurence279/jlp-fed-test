import ProductDetails from "../../components/product-details/product-details";
import {detailsData} from "../../mockData/data2.json"
import {products} from "../../mockData/data.json"

// export async function getServerSideProps(context) {
//   const id = context.params.id;
//   const response = await fetch(
//     "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
//   );
//   const data = await response.json();

//   return {
//     props: { data },
//   };
// }

export async function getServerSideProps(context) {

  // Use the ID passed to find the matching product.

  const id = context.params.id;
  let data = detailsData.find((product) => {
    return product.productId === id
  })



  if(!data) 
  {
    data = products.find((product) => {
      return product.productId === id
    })
  }

  return {
    props: { data },
  };

}



const ProductDetail = ({ data }) => {
  return (
    <div>
      <h1 data-testid="title" dangerouslySetInnerHTML={{ __html: data.title }}></h1>
      <ProductDetails data={data} />
    </div>
  );
};

export default ProductDetail;
