import * as React from 'react'
import {render} from "@testing-library/react";
import '@testing-library/jest-dom'
import ProductListItem from "../components/product-list-item/product-list-item";
import data from "../mockData/data2.json"
import ProductDetail from "../pages/product-detail/[id]"

describe("Product page tests..", () => {

    it("Should render a single product page", ()=>{ 
        const {getByTestId} = render(<ProductListItem/>) 
        const renderedItem = getByTestId("product-list-item");
        expect(renderedItem).toBeInTheDocument() 
    })

    it("Should display the correct product based on a random ID", () => {

        // Get a random product, and ensure the JSX contains the correct info. 
        const rand_index = Math.floor(Math.random() * data.detailsData.length - 1 );
        const product = data.detailsData[rand_index];

        const {getByTestId} = render(<ProductDetail data={product}/>)

        expect(getByTestId("title")).toBeInTheDocument();
        expect(getByTestId("price")).toBeInTheDocument();
        expect(getByTestId("special-offer")).toBeInTheDocument();
        expect(getByTestId("services")).toBeInTheDocument();
        expect(getByTestId("features")).toBeInTheDocument();

    })
    

})
