import * as React from 'react'
import {render} from "@testing-library/react";
import '@testing-library/jest-dom'
import Home from "../pages/index";
import data from "../mockData/data.json"

describe("Front page tests..", () => {


    it("Should render the home-page", ()=>{ 
        const {getByTestId} = render(<Home data={data} />) 
        const renderedItem = getByTestId("home-page");
        expect(renderedItem).toBeInTheDocument() 
    })
    
    it("Should display at least one product/dishwasher item", () => {
        const {getAllByTestId} = render(<Home data={data} />) 
        const renderedItems = getAllByTestId("product-list-item");
        expect(renderedItems.length).toBeGreaterThan(0)
    })
    
    it("Should display no more than 20 products/dishwashers in the list of items",()=>{
        const expectedLength = 21;
        const {getByTestId} = render(<Home data={data} />) 
        const renderedItems = getByTestId("product-list-items");
        expect(renderedItems.children.length).toBeLessThan(expectedLength)
    })
    
    
    
    

})
